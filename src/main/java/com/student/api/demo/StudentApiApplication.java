package com.student.api.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentApiApplication.class, args);
	}
	
	
/** _endpoints
GET ALL RESOURCES
http://localhost:8080/students

GET SINGLE RESOURCE
http://localhost:8080/students/{id}

POST CREATE NEW RESOURCE 
http://localhost:8080/students

PUT - Update existing resource or create new if does not exist 
http://localhost:8080/students/{id}

PATCH PARTIAL UPDATE 
http://localhost:8080/students/{id}

DELETE SINGLE RESOURCE
http://localhost:8080/students/{id}

DELETE ALL RESOURCE
http://localhost:8080/students/
**/
	
}
