package com.student.api.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.student.api.demo.error.RecordNotFoundException;
import com.student.api.demo.model.Student;
import com.student.api.demo.serivce.StudentService;

@RestController
public class StudentController {

	@Autowired
	StudentService service;

	// Get all students
	// http://localhost:8080/students
	@GetMapping("/students")
	public List<Student> getAllStudents() {
		return service.findAll();

	}

	// Add Student
	// http://localhost:8080/students
	@PostMapping("/students")
	public ResponseEntity<Student> addUser(@RequestBody Student student) {
		System.out.println("AddUser");
		Student newStudent = service.save(student);
		return new ResponseEntity<Student>(newStudent, HttpStatus.CREATED);
	}

	// GET Single Student
	// http://localhost:8080/students/1
	@GetMapping(path = "/students/{id}")
	public ResponseEntity<Student> getStudent(@PathVariable("id") Integer id) {
		Optional<Student> student = service.findById(id);
		if (!student.isPresent()) {
			throw new RecordNotFoundException("Student " + id + " not found.");
		}

		return new ResponseEntity<Student>(student.get(), HttpStatus.FOUND);
	}

	
	// Update student
	// http://localhost:8080/students/{id}
	@PutMapping(path = "/students/{id}")
	public Student updateStudent(@RequestBody Student student, @PathVariable("id") Integer id) {
		return service.updateStudentById(student, id);
	}
	
	// Patch - partial update specific column
		@PatchMapping(path = "/students/{id}")
		public ResponseEntity<Student> updateStudentEmailorName(@RequestBody Student student,@PathVariable("id") Integer id) {

			Optional<Student> currentStudent = service.findById(id);
			Student updateStudentData = new Student();
			if (!currentStudent.isPresent()) {
				throw new RecordNotFoundException("Student " + id + " not found.");
			} else {
				updateStudentData = currentStudent.get();
				if (student.getName() != null && student.getName() != "") {
					updateStudentData.setName(student.getName());
				}
				if (student.getEmail() != null && student.getEmail() != "") {
					updateStudentData.setEmail(student.getEmail());
				}
				return new ResponseEntity<Student>(service.save(updateStudentData), HttpStatus.OK);
			}
		}
		
		// Delete Single Student
		// http://localhost:8080/students/1
		@DeleteMapping(path = "/students/{id}")
		public ResponseEntity<String> deleteStudent(@PathVariable("id") Integer id) {
			boolean isUserExists = service.checkUserExists(id);

			if (!isUserExists) {
				throw new RecordNotFoundException("Student " + id + " not found.");
			} else {
				service.deleteStudentById(id);
			}

			return new ResponseEntity<String>("Student " + id + " deleted successfully", HttpStatus.OK);

		}
		
		
		// Delete All Students
		// http://localhost:8080/students
		@DeleteMapping("/students")
		public String deleteAll() {
			return service.deleteAll();
		}
		
}
