package com.student.api.demo.serivce;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.api.demo.model.Student;
import com.student.api.demo.repo.StudentRepository;

@Service
public class StudentService {

	@Autowired
	StudentRepository repo;

	public List<Student> findAll() {
		return repo.findAll();
	}

	public Student save(Student student) {
		return repo.save(student);
	}

	public Optional<Student> findById(Integer id) {
		return repo.findById(id);
	}
	
	public boolean checkUserExists(Integer id) {
		return repo.existsById(id);
	}
	
	public Student updateStudentById(Student student, Integer id) {
		Student updateStudent = new Student();
		updateStudent.setId(id);
		updateStudent.setName(student.getName());
		updateStudent.setEmail(student.getEmail());
		return repo.save(updateStudent);
	}
	
	
	public boolean deleteStudentById(Integer id) {
		
		repo.deleteById(id);
		
		return true;
	}
	
	public String deleteAll() {
		repo.deleteAll();
		return "All records deleted";
	}
	
}
