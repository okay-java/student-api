package com.student.api.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.student.api.demo.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>{

}
